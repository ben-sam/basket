# Basket Application
### A simple shopping list application


Created by Benjamin Sam.


## How do I run this?

1. Clone this repo using `git clone --depth=1 https://bitbucket.org/ben-sam/basket.git`
2. Move to the appropriate directory: `cd basket`.
3. Run `yarn install` in order to install dependencies.
4. Run `yarn start` to launch the application locally at `http://localhost:3000`.
