import styled from 'styled-components';

export default styled.div`
  padding: 2rem;
  
  .basket{
    padding: 2rem;
    
    .inBasket {
      text-decoration: line-through;
      opacity: 0.4;
    }
    
    .viz {
      width: 100%;
      text-align: center;
    }
  }
  `;
