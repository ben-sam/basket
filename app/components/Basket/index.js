import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import injectReducer from 'utils/injectReducer';
import reducer from '../../containers/HomePage/reducer';
import * as actions from '../../containers/HomePage/actions';
import * as selectors from '../../containers/HomePage/selectors';

import DonutChart from "../DonutChart";

import {Card, CardHeader, CardText, CardActions} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton';


import Style from './style.js';

export class Basket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.clearBasket = this.clearBasket.bind(this);
    this.returnToCart = this.returnToCart.bind(this);
  }

  returnToCart(event) {
    let itemId = event.target.getAttribute("value");
    let basketList = this.props.basketList;
    let shoppingList = this.props.shoppingList;

    for (let x = 0; x < basketList.length; x++) {
      if (basketList[x].id === +itemId) {

        shoppingList.push(basketList[x]);
        basketList.splice(x,1);
      }
    }

    this.props.addToBasket(shoppingList, basketList);

  }

  clearBasket() {
    this.props.clearBasket()
  }

  render() {
    return (
      <Style {...this.props}>
        <Card className="basket">
          <CardText>
            <h3>Items in Basket</h3>
            <List className="inBasket">
              {
                this.props.basketList.map((item,i) => {
                  return (
                    <ListItem key={item.id}
                              className="boughtItem"
                              value={item.id}
                              primaryText={item.name}
                              leftIcon={<Checkbox checked={true} value={item.id} onClick={this.returnToCart}/>}
                              secondaryText={"Quantity: " + item.qty}
                              rightIcon={<i value={item.id} className="material-icons"
                                            onClick={this.returnToCart}>undo</i>}/>
                  )
                })
              }
            </List>
            <div className="viz">
              <DonutChart className="donut"/>
            </div>
          </CardText>
          <CardActions>
            <FlatButton label="Clear Basket"
                        onClick={this.clearBasket}
                        disabled={this.props.basketList.length < 1}/>
          </CardActions>
        </Card>
      </Style>
    )
  }
}

Basket.defaultProps = {};

Basket.propTypes = {};

const mapStateToProps = createStructuredSelector({
  shoppingList: selectors.makeSelectShoppingList(),
  basketList: selectors.makeSelectBasketList()
});

export function mapDispatchToProps(dispatch) {
  return {
    addToBasket: (shoppingList, basketList) => {
      dispatch(actions.addToBasket(shoppingList, basketList))
    },
    clearBasket: () => {
      dispatch(actions.clearBasket())
    }
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'home', reducer});

export default compose(withReducer, withConnect)(Basket);
