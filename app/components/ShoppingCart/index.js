import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import injectReducer from 'utils/injectReducer';
import reducer from '../../containers/HomePage/reducer';
import * as actions from '../../containers/HomePage/actions';
import * as selectors from '../../containers/HomePage/selectors';

import {Card, CardHeader, CardText, CardActions} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton'

import Style from './style.js';
import injectSaga from "../../utils/injectSaga";

export class ShoppingCart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.deleteItem = this.deleteItem.bind(this);
    this.listClick = this.listClick.bind(this);
  }

  deleteItem(event) {
    event.stopPropagation();

    let itemId = event.target.getAttribute("value");
    let shoppingList = this.props.shoppingList;

    for (let x = 0; x < shoppingList.length; x++) {
      if (shoppingList[x].id === +itemId) {
        shoppingList.pop(x);
      }
    }
    this.props.updateCart(shoppingList);
  }

  listClick(event) {
    let itemId = event.target.getAttribute("value");
    let basketList = this.props.basketList;
    let shoppingList = this.props.shoppingList;

    for (let x = 0; x < shoppingList.length; x++) {
      if (shoppingList[x].id === +itemId) {
        basketList.push(shoppingList[x]);
        shoppingList.splice(x,1)
      }
    }

    this.props.addToBasket(shoppingList, basketList);
  }

  render() {
    return (
      <Style {...this.props}>
        <div className="title">
          {this.props.shoppingList.length > 0 ?
            <h3>Shopping List</h3> :
            <h3></h3>}
        </div>
        <List>
          {
            this.props.shoppingList.map((item, i) => {
              return (
                <ListItem key={item.id}
                          primaryText={item.name}
                          secondaryText={"Quantity: " + item.qty}
                          leftIcon={<Checkbox value={item.id} onClick={this.listClick}/>}
                          rightIcon={<i value={item.id} className="material-icons" onClick={this.deleteItem}>delete</i>}
                />)
            })
          }
        </List>
      </Style>
    )
  }
}

ShoppingCart.defaultProps = {};

ShoppingCart.propTypes = {};

const mapStateToProps = createStructuredSelector({
  shoppingList: selectors.makeSelectShoppingList(),
  basketList: selectors.makeSelectBasketList()
});

export function mapDispatchToProps(dispatch) {
  return {
    updateCart: (list) => {
      dispatch(actions.updateCart(list))
    },
    addToBasket: (shoppingList, basketList) => {
      dispatch(actions.addToBasket(shoppingList, basketList));
    }
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'home', reducer});

export default compose(withReducer, withConnect)(ShoppingCart);
