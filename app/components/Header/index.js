import React from 'react';
import PropTypes from 'prop-types';
import Style from './style.js';

import AppBar from 'material-ui/AppBar';

export class Header extends React.Component {
  constructor(props){
    super(props);
    this.state = {};
  }

  render(){
    return(
      <Style {...this.props}>
        <AppBar title="Basket" iconElementLeft={<div/>}/>
      </Style>
    )
  }
}

Header.defaultProps = {

};

Header.propTypes = {

};

export default Header;
