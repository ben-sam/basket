import styled from 'styled-components';

export default styled.div`
  padding: 2rem;
  
  .card {
    padding: 2rem;
    
    .itemRow{
      display: flex;
      align-items: center;
      
      .itemField {
        margin: 0 1rem;
      }
    }
  }
`;
