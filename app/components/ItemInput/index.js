import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import injectReducer from 'utils/injectReducer';
import reducer from '../../containers/HomePage/reducer';
import * as actions from '../../containers/HomePage/actions';
import * as selectors from '../../containers/HomePage/selectors';

import {Card, CardHeader, CardText, CardActions} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

import Style from './style.js';
import ShoppingCart from "../ShoppingCart";

export class ItemInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemName: {
        value: "",
        error: "",
        disableButton: true
      },
      itemQty: {
        value: "",
        error: "",
        disableButton: true
      }
    };

    this.addToCart = this.addToCart.bind(this);
    this.editItemName = this.editItemName.bind(this);
    this.editItemQty = this.editItemQty.bind(this);
    this.cancelInput = this.cancelInput.bind(this);
  }


  addToCart() {
    let item = {
      id: Math.random(),
      name: this.state.itemName.value,
      qty: this.state.itemQty.value
    };
    this.props.addToCart(item);
    this.setState({
      'itemName': {value: "", error: "", disableButton: true},
      'itemQty': {value: "", error: "", disableButton: true}
    })
  }

  editItemName(event, newValue) {
    let regexp = RegExp('^[a-zA-Z0-9]+$');
    let itemState = {
      value: newValue,
      error: "",
      disableButton: true
    };

    let errors = [];

    if (!newValue.length > 0) {
      errors.push("Enter an item name");
    }
    if (!regexp.test(newValue)) {
      errors.push("Enter alphanumeric text only");
    }

    if (errors.length > 0) {
      itemState.error = errors.join(", ");
      itemState.disableButton = true;
    } else {
      itemState.value = newValue;
      itemState.disableButton = false;
    }

    this.setState({'itemName': itemState});
  }

  editItemQty(event, newValue) {
    let regexp = RegExp('^[0-9]+$');
    let itemState = {
      value: newValue,
      error: "",
      disableButton: true
    };

    let errors = [];

    if (!regexp.test(newValue)) {
      errors.push("Enter a number");
    }

    if (!parseInt(newValue) && newValue !== "0") {
      errors.push("Enter a whole number")
    }
    if (parseInt(newValue) <= 0) {
      errors.push("Enter a number greater than 0")
    }

    if (errors.length > 0) {
      itemState.error = errors.join(", ");
      itemState.disableButton = true;
    } else {
      itemState.value = newValue;
      itemState.disableButton = false;
    }

    this.setState({'itemQty': itemState});
  }

  cancelInput() {
    let cleared = {
      value: "",
      error: "",
      disableButton: true
    };

    this.setState({
      'itemName': cleared,
      'itemQty': cleared
    })
  }

  render() {
    return (
      <Style {...this.props}>
        <Card className="card">
          <CardText>
            <div className="shopping-list">
              <ShoppingCart/>
            </div>
            <div className="title">
              <h3>What item do you need?</h3>
            </div>
            <div className="itemRow">
                <TextField id="itemName"
                           className="itemField"
                           hintText="Strawberries"
                           fullWidth={true}
                           floatingLabelText="What Item?"
                           errorText={this.state.itemName.error}
                           value={this.state.itemName.value}
                           onChange={this.editItemName}/>
                <TextField id="itemQty"
                           className="itemField"
                           hintText="5"
                           fullWidth={true}
                           floatingLabelText="How many?"
                           errorText={this.state.itemQty.error}
                           value={this.state.itemQty.value}
                           onChange={this.editItemQty}/>
            </div>
          </CardText>
          <CardActions>
            <FlatButton label="Add Item"
                        onClick={this.addToCart}
                        disabled={this.state.itemName.disableButton || this.state.itemQty.disableButton}/>
            <FlatButton label="Cancel" onClick={this.cancelInput}/>
          </CardActions>
        </Card>
      </Style>
    )
  }
}

ItemInput.defaultProps = {};

ItemInput.propTypes = {};

const mapStateToProps = createStructuredSelector({
  shoppingList: selectors.makeSelectShoppingList(),
  basketList: selectors.makeSelectBasketList()
});

export function mapDispatchToProps(dispatch) {
  return {
    addToCart: (item) => {
      dispatch(actions.addToList(item))
    }
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'home', reducer});

export default compose(withReducer, withConnect)(ItemInput);
