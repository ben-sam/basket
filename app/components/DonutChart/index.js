import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import injectReducer from 'utils/injectReducer';
import reducer from '../../containers/HomePage/reducer';
import * as actions from '../../containers/HomePage/actions';
import * as selectors from '../../containers/HomePage/selectors';

import * as d3 from 'd3';

import Style from './style.js';

export class DonutChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.data = [];

    this.cfg = {
      svg: {width: 200, height: 200},
      radius: 100,
      donutWidth: 25
    };

    this.color = d3.scaleOrdinal(d3.schemeCategory10);

    this.svg = {};
  }

  componentWillMount() {
    this.data = [
      {
        name: "In Basket",
        value: this.calcQty(this.props.basketList)
      },
      {
        name: "In List",
        value: this.calcQty(this.props.shoppingList)
      }
    ];
  }

  shouldComponentUpdate(nextProps) {
    return JSON.stringify(this.props.shoppingList) !== JSON.stringify(nextProps.shoppingList) ||
      JSON.stringify(this.props.basketList !== JSON.stringify(nextProps.basketList));
  }

  componentWillUpdate(nextProps) {
    let that = this;

    this.data = [
      {
        name: "In Basket",
        value: this.calcQty(nextProps.basketList)
      },
      {
        name: "In List",
        value: this.calcQty(nextProps.shoppingList)
      }
    ];


    this.arc = d3.arc()
      .innerRadius(this.cfg.radius - this.cfg.donutWidth)
      .outerRadius(this.cfg.radius);

    this.chart.selectAll('path').remove();
    this.chart.selectAll('.legend').remove();

    let inBasketQty = this.data[0].value;
    let inShoppingListQty = this.data[1].value;
    this.numInBasket = Math.round((inBasketQty * 100 / (inBasketQty + inShoppingListQty)));

    if (!isNaN(this.numInBasket)) {
      this.drawChart(this.chart);
    }

  }

  componentDidMount() {

    let that = this;

    this.chart = d3.select(this.svg)
      .attr('width', this.cfg.svg.width)
      .attr('height', this.cfg.svg.height)
      .append('g')
      .attr('transform', 'translate(' + (this.cfg.svg.width / 2) + ',' + (this.cfg.svg.height / 2) + ')');

    let inBasketQty = this.data[0].value;
    let inShoppingListQty = this.data[1].value;
    this.numInBasket = Math.round((inBasketQty * 100 / (inBasketQty + inShoppingListQty)));

    if (!isNaN(this.numInBasket)) {
      this.drawChart(this.chart);
    }
  }

  drawChart(chart) {
    let that = this;
    this.arc = d3.arc()
      .innerRadius(this.cfg.radius - this.cfg.donutWidth)
      .outerRadius(this.cfg.radius);

    this.pie = d3.pie().value(function (d) {
      return d.value
    }).sort(null);

    chart.selectAll('path')
      .data(this.pie(this.data))
      .enter()
      .append('path')
      .attr('d', this.arc)
      .attr('fill', function (d, i) {
        return that.color(d.data.name);
      });

    let legendText = (isNaN(this.numInBasket) ? "There are no items in " : this.numInBasket + "% in Basket");

    let legend = this.chart
      .append('g')
      .attr('class', 'legend');

    legend.append('text')
      .attr('dy', '.3em')
      .text(legendText)
      .style('text-anchor', 'middle');
  }

  calcQty(arr) {
    let qty = 0;
    for (let x = 0; x < arr.length; x++) {
      qty += +arr[x]["qty"];
    }

    return qty;
  }


  render() {
    return (
      <Style {...this.props}>
        <svg ref={(svg) => this.svg = svg}></svg>
      </Style>
    )
  }
}

DonutChart.defaultProps = {};

DonutChart.propTypes = {};

const mapStateToProps = createStructuredSelector({
  shoppingList: selectors.makeSelectShoppingList(),
  basketList: selectors.makeSelectBasketList()
});

export function mapDispatchToProps(dispatch) {
  return {
    addToCart: (item) => {
      dispatch(actions.addToList(item))
    }
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({key: 'home', reducer});

export default compose(withReducer, withConnect)(DonutChart);
