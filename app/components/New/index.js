import React from 'react';
import PropTypes from 'prop-types';

import Style from './style.js';

export class RENAME_THIS_COMPONENT extends React.Component {
  constructor(props){
    super(props);
    this.state = {};
  }

  render(){
    return(
      <Style {...this.props}>
      </Style>
    )
  }
}

RENAME_THIS_COMPONENT.defaultProps = {

};

RENAME_THIS_COMPONENT.propTypes = {

};

export default RENAME_THIS_COMPONENT;
