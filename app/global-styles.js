import { injectGlobal } from 'styled-components';
import robotoFont from 'assets/fonts/Roboto/Roboto-Regular.ttf';

/* eslint no-unused-expressions: 0 */
injectGlobal`

  @font-face {
    font-family: 'Roboto';
    src: url(${robotoFont}) format('truetype');
  }

  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Roboto', Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Roboto', Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: 'Roboto', Arial, sans-serif;
    line-height: 1.5em;
  }
`;
