export const ADD_TO_LIST = 'basket/HomePage/ADD_TO_LIST';
export const UPDATE_LIST = 'basket/HomePage/UPDATE_LIST';
export const ADD_TO_BASKET = 'basket/HomePage/ADD_TO_BASKET';
export const CLEAR_BASKET = 'basket/HomePage/CLEAR_BASKET';
