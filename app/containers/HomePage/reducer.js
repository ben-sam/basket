import {fromJS} from 'immutable';

import * as constants from './constants';

const initialState = fromJS({
  shoppingList: [],
  basketList: []
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case constants.ADD_TO_LIST:
      let shoppingList = state.get('shoppingList').toJS();
      shoppingList.push(action.item);
      return state.set('shoppingList', fromJS(shoppingList));
    case constants.UPDATE_LIST:
      return state.set('shoppingList', fromJS(action.list));
    case constants.ADD_TO_BASKET:
      let update1 = state.set('shoppingList', fromJS(action.shoppingList));
      return update1.set('basketList', fromJS(action.basketList));
    case constants.CLEAR_BASKET:
      return state.set('basketList', fromJS([]));
    default:
      return state;
  }
}

export default appReducer;
