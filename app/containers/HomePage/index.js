/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import ItemInput from "../../components/ItemInput/index";
import ShoppingCart from "../../components/ShoppingCart";

import Style from './style.js';
import Basket from "../../components/Basket";

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Style {...this.props}>
        <h1>
          <div className="input-item">
            <ItemInput/>
          </div>
          <div className="basket">
            <Basket/>
          </div>
        </h1>
      </Style>
    );
  }
}
