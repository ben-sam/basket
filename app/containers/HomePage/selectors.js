import {createSelector} from 'reselect';

const selectHomeState = (state) => {
  return state.get('home');
};

const makeSelectShoppingList = () => createSelector(
  selectHomeState, (homeState) => {
    return homeState.get('shoppingList').toJS();
  }
);

const makeSelectBasketList = () => createSelector(
  selectHomeState, (homeState) => {
    return homeState.get('basketList').toJS();
  }
);

export {makeSelectShoppingList, makeSelectBasketList}
