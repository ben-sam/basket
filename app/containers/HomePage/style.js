import styled from 'styled-components';

export default styled.div`
  width: 100%;
  
  .input-item {
    max-width: 60rem;
    margin: auto;
  }
  
  .basket {
    max-width: 60rem;
    margin: auto;
  }
`;
