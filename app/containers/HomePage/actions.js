import * as constants from './constants';

export function addToList(item){
  return {
    type: constants.ADD_TO_LIST,
    item
  }
}

export function updateCart(list){
  return {
    type: constants.UPDATE_LIST,
    list
  }
}

export function addToBasket(shoppingList, basketList){
  return {
    type: constants.ADD_TO_BASKET,
    shoppingList,
    basketList
  }
}

export function clearBasket(){
  return {
    type: constants.CLEAR_BASKET
  }
}
